<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>Alarm</name>
    <message>
        <source>Appointment reminder(s)</source>
        <translation>Напоминания</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <source>or select item</source>
        <translation>или выберите из списка</translation>
    </message>
    <message>
        <source>Dismiss all</source>
        <translation>Прекратить всё</translation>
    </message>
    <message>
        <source>Dismiss</source>
        <translation>Прекратить</translation>
    </message>
</context>
<context>
    <name>ApptModel</name>
    <message>
        <source>Overdue by </source>
        <translation>Просрочено </translation>
    </message>
    <message>
        <source>In </source>
        <translation>Через</translation>
    </message>
    <message>
        <source>%1 days</source>
        <translation>%1 дней</translation>
    </message>
    <message>
        <source>%1 hours</source>
        <translation type="obsolete">%1 ч</translation>
    </message>
    <message>
        <source>%1 minutes</source>
        <translation>%1 м</translation>
    </message>
    <message>
        <source>Subject</source>
        <translation>Тема</translation>
    </message>
    <message>
        <source>When</source>
        <translation>Когда</translation>
    </message>
    <message>
        <source>Expires</source>
        <translation>Срок истекает</translation>
    </message>
    <message>
        <source>Location</source>
        <translation>Место</translation>
    </message>
    <message>
        <source>name</source>
        <translation>Тема</translation>
    </message>
    <message>
        <source>loc</source>
        <translation>Место</translation>
    </message>
    <message>
        <source>%1 hours,</source>
        <translation>%1 ч,</translation>
    </message>
    <message>
        <source>&lt;div style=&quot;font-weight: bold; color: red;&quot;&gt;Overdue by </source>
        <translation>&lt;div style=&quot;font-weight: bold;color: red;&quot;&gt;Просрочено </translation>
    </message>
    <message>
        <source>&lt;div style=&quot;font-weight: bold; color: green;&quot;&gt;In </source>
        <translation>&lt;div style=&quot;font-weight: bold;color: green;&quot;&gt;Через </translation>
    </message>
</context>
<context>
    <name>Messages</name>
    <message>
        <source>New mail</source>
        <translation>Новые сообщения</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <source>Add to filter</source>
        <translation type="obsolete">Добавить в фильтр</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <source>Mark as read</source>
        <translation type="obsolete">Пометить как прочитанные</translation>
    </message>
    <message>
        <source>To filter</source>
        <translation>В фильтр</translation>
    </message>
    <message>
        <source>Mark</source>
        <translation>Пометить</translation>
    </message>
</context>
<context>
    <name>Messmodel</name>
    <message>
        <source>â</source>
        <translation></translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <source>From</source>
        <translation type="obsolete">От</translation>
    </message>
    <message>
        <source>Subject</source>
        <translation type="obsolete">Тема</translation>
    </message>
    <message>
        <source>Fragment</source>
        <translation>Фрагмент</translation>
    </message>
    <message>
        <source>From/Subject/Fragment</source>
        <translation>От/Заголовок/Фрагмент</translation>
    </message>
    <message>
        <source>Subject - fragment</source>
        <translation>Заголовок - фрагмент</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>I couldn&apos;t detect any system tray on this system.</source>
        <translation>Не удалось обнаружить системный трей на этой системе.</translation>
    </message>
    <message>
        <source>Systray</source>
        <translation>Системный трей</translation>
    </message>
</context>
<context>
    <name>Zebra</name>
    <message>
        <source>Zebra</source>
        <translation type="obsolete">Зебра</translation>
    </message>
    <message>
        <source>Zebra - configuration </source>
        <oldsource>Zebra - zimbra mail notification</oldsource>
        <translation>Зебра - настройка подключения к зимбре </translation>
    </message>
    <message>
        <source>myzimbraserver.com</source>
        <translation>ваш почтовый сервер</translation>
    </message>
    <message>
        <source>Server:</source>
        <oldsource>Server</oldsource>
        <translation>Сервер:</translation>
    </message>
    <message>
        <source>usermail@myzimbraserver.com</source>
        <translation>user@server.ru</translation>
    </message>
    <message>
        <source>Login:</source>
        <oldsource>Login</oldsource>
        <translation>Учетная запись:</translation>
    </message>
    <message>
        <source>Password:</source>
        <oldsource>Password</oldsource>
        <translation>Пароль:</translation>
    </message>
    <message>
        <source>Default browser</source>
        <translation>по умолчанию, либо укажите. Пример: chrome.exe --app=</translation>
    </message>
    <message>
        <source>Browser</source>
        <translation>Браузер</translation>
    </message>
    <message>
        <source>Black list</source>
        <translation>Черный список</translation>
    </message>
    <message>
        <source>Timeout:</source>
        <translation>Проверять каждые:</translation>
    </message>
    <message>
        <source>sec.</source>
        <translation>с</translation>
    </message>
    <message>
        <source>sec</source>
        <translation type="obsolete">секунд(ы)</translation>
    </message>
    <message>
        <source>Sound</source>
        <translation>Звук</translation>
    </message>
    <message>
        <source>ignore SSL errors</source>
        <translation>игнорировать SSL</translation>
    </message>
    <message utf8="true">
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/images/zebra03.svg&quot; /&gt; &lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:14pt; font-weight:600;&quot;&gt;Zebra &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:1px; margin-bottom:1px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Zimbra tray notification application&lt;/p&gt;
&lt;p style=&quot; margin-top:1px; margin-bottom:1px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;;&quot;&gt;Version: 0.1&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:1px; margin-bottom:1px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;;&quot;&gt;License: GPL v2.+&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:10px; margin-bottom:1px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;©&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;;&quot;&gt; 2012-2013 Dmitry Ermolenko &amp;lt;deep1@list.ru&amp;gt;&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/images/zebra03.svg&quot; /&gt; &lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:14pt; font-weight:600;&quot;&gt;Зебра &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:1px; margin-bottom:1px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Получайте уведомления о новых сообщениях Zimbra&lt;/p&gt;
&lt;p style=&quot; margin-top:1px; margin-bottom:1px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;;&quot;&gt;Версия: 0.1&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:1px; margin-bottom:1px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;;&quot;&gt;Лицензия: GPL v2.+&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:10px; margin-bottom:1px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;©&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;;&quot;&gt; 2012-2013 Ермоленко Дмитрий &amp;lt;deep1@list.ru&amp;gt;&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <source>Timeout checks</source>
        <translation type="obsolete">Проверять каждые</translation>
    </message>
    <message>
        <source>Zimbra notification</source>
        <oldsource>Zimbra Tray notification</oldsource>
        <translation>Зимбра - почта</translation>
    </message>
    <message>
        <source>Mi&amp;nimize</source>
        <translation type="obsolete">&amp;Скрыть</translation>
    </message>
    <message>
        <source>Ma&amp;ximize</source>
        <translation>&amp;Параметры</translation>
    </message>
    <message>
        <source>&amp;Hide</source>
        <translation>&amp;Скрыть</translation>
    </message>
    <message>
        <source>White list</source>
        <translation>Белый список</translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation>&amp;Настройки</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation>&amp;Выход</translation>
    </message>
    <message>
        <source>&amp;View mail</source>
        <translation>&amp;Смотреть почту</translation>
    </message>
    <message>
        <source>New messages %2 %1
%3</source>
        <translation>Новых сообщений %2 %1
%3</translation>
    </message>
    <message>
        <source>Zimbra (new messages: %2 %1)</source>
        <translation type="obsolete">Зимбра (новые сообщения: %2 %1)</translation>
    </message>
    <message>
        <source>&lt;none&gt;</source>
        <translation type="obsolete">&lt;нет&gt;</translation>
    </message>
    <message>
        <source>Error:</source>
        <translation>Ошибка:</translation>
    </message>
    <message>
        <source>Subject: </source>
        <translation>Заголовок: </translation>
    </message>
    <message>
        <source>From: %1
%2
</source>
        <translation>От: %1
%2
</translation>
    </message>
    <message>
        <source>more than</source>
        <translation>более</translation>
    </message>
    <message>
        <source>Zimbra (new messages: %2%1)</source>
        <translation type="obsolete">Зимбра (новых сообщений: %2 %1)</translation>
    </message>
    <message>
        <source>New messages %2 %1</source>
        <translation>Новых сообщений %2 %1</translation>
    </message>
    <message>
        <source>Zimbra (new messages: %1)</source>
        <translation type="obsolete">Зимбра (новые сообщения: %1)</translation>
    </message>
    <message>
        <source>Zimbra Tray (messages %1)</source>
        <translation type="obsolete">Зимбра (новых сообщений %1)</translation>
    </message>
    <message>
        <source>New messages: %1</source>
        <oldsource>New message: %1</oldsource>
        <translation type="obsolete">Новых сообщений: %1</translation>
    </message>
    <message>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <source>AutoRun</source>
        <translation>Автостарт</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <source>Disable autorun</source>
        <translation>Отключить автостарт</translation>
    </message>
    <message>
        <source>Enable autorun</source>
        <translation>Включить автостарт</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Start browser in &lt;span style=&quot; font-style:italic;&quot;&gt;application mode e&lt;/span&gt;xamples:&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Courier New,courier&apos;; font-size:11px; color:#2a2a2a;&quot;&gt;chrome.exe --app=$mail_server$&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Courier New,courier&apos;; font-size:11px; color:#2a2a2a;&quot;&gt;firefox.exe -P User_profile -no-remote&lt;/span&gt;&lt;/p&gt;&lt;pre style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Courier New,courier&apos;; font-size:11px; color:#2a2a2a;&quot;&gt;iexplore.exe -k&lt;/span&gt;&lt;/pre&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Запустить браузер в &lt;span style=&quot; font-style:italic;&quot;&gt;режиме приложения &lt;/span&gt;примеры:&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Courier New,courier&apos;; font-size:11px; color:#2a2a2a;&quot;&gt;chrome.exe --app=$mail_server$&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Courier New,courier&apos;; font-size:11px; color:#2a2a2a;&quot;&gt;firefox.exe -P User_profile -no-remote&lt;/span&gt;&lt;/p&gt;&lt;pre style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Courier New,courier&apos;; font-size:11px; color:#2a2a2a;&quot;&gt;iexplore.exe -k&lt;/span&gt;&lt;/pre&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Window</source>
        <translation>Окно</translation>
    </message>
    <message utf8="true">
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/images/zebra03.svg&quot; /&gt; &lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:14pt; font-weight:600;&quot;&gt;Zebra &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:1px; margin-bottom:1px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Zimbra tray notification application&lt;/p&gt;
&lt;p style=&quot; margin-top:1px; margin-bottom:1px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;;&quot;&gt;Version: 0.5.0 Beta&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:1px; margin-bottom:1px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;;&quot;&gt;License: Donateware&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;right&quot; style=&quot; margin-top:1px; margin-bottom:1px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Webmoney:      R394707885416 &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;right&quot; style=&quot; margin-top:10px; margin-bottom:1px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Z395243942271&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:1px; margin-bottom:1px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://sites.google.com/site/zebrazimbramail/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://zebra.ermoler.ru&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:10px; margin-bottom:1px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;©&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;;&quot;&gt; 2012-2013 Dmitry Ermolenko &amp;lt;deep1@list.ru&amp;gt;&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:10px; margin-bottom:1px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;View browser</source>
        <translation>Открыть &amp;браузер</translation>
    </message>
</context>
</TS>
